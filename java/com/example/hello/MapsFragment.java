package com.example.hello;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.preference.PreferenceManager;
import android.location.Address;
import android.location.Geocoder;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.MapView;

import java.io.IOException;
import java.util.List;

public class MapsFragment extends Fragment {

    private MapView map;
    private EditText searchEditText;
    private Button searchButton;
    private Geocoder geocoder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);

        // Load/initialize the osmdroid configuration
        Configuration.getInstance().load(getActivity(), PreferenceManager.getDefaultSharedPreferences(getActivity()));

        // Initialize the map
        map = view.findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        // Initialize the search components
        searchEditText = view.findViewById(R.id.searchEditText);
        searchButton = view.findViewById(R.id.searchButton);
        geocoder = new Geocoder(getActivity());

        setMapLocation(-6.2047, 106.7822);

        // Set an onClickListener for the search button
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchLocation();
            }
        });

        return view;
    }

    private void setMapLocation(double latitude, double longitude) {
        map.getController().setCenter(new org.osmdroid.util.GeoPoint(latitude, longitude));
        map.getController().setZoom(10.0);
        map.invalidate();
    }

    private void searchLocation() {
        String locationName = searchEditText.getText().toString();

        try {
            List<Address> addresses = geocoder.getFromLocationName(locationName, 1);
            if (!addresses.isEmpty()) {
                Address address = addresses.get(0);
                double latitude = address.getLatitude();
                double longitude = address.getLongitude();
                setMapLocation(latitude, longitude);
            } else {
                Toast.makeText(getActivity(), "Location not found", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        map.onResume(); // Needed for compass, my location overlays, v6.0.0 and up
    }

    @Override
    public void onPause() {
        super.onPause();
        map.onPause(); // Needed for compass, my location overlays, v6.0.0 and up
    }
}
